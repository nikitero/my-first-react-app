import { BrowserRouter as Router } from "react-router-dom";
import './App.css';
import Header from './components/Header/Header';
import Navbar from './components/Navbar/Navbar';
import Main from './components/Main/Main';

function App() {




  return (
    <div className="App">
  <Router>
      <Header/>
      <Navbar/>
      <Main/>
  </Router>

    </div>
  );
}

export default App;
