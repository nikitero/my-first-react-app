import React from 'react'
import { ChakraProvider } from '@chakra-ui/react'
import {
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    AccordionIcon,
    Box,
    Text,
    Heading,
    Image,
    Center
  } from '@chakra-ui/react'

const AboutUs = () => {
  return (
<div className = "about-us">
    <ChakraProvider>
      <Accordion allowToggle>
        <AccordionItem>
          <h2>
            <AccordionButton _expanded={{ bg: 'gray', color: 'white' }}>
                <Box as="span" flex='1' textAlign='center'>
                  <Heading>About Us</Heading>
                </Box>
                <AccordionIcon />
            </AccordionButton>
          </h2>
            <AccordionPanel pb={4}>
              <Text fontSize='2xl' color='azure'>My Name is Niki and I am a cocktail lover</Text>
              <Center>
                <Image
                      borderRadius='full'
                      boxSize='150px'
                      src='https://media.licdn.com/dms/image/C4D03AQFNEjzpNAiDjA/profile-displayphoto-shrink_800_800/0/1625161185531?e=2147483647&v=beta&t=6Xr8rc2uJhkgf0gCjYu3q7LdFXHXuz5fz_u4x-pAfhM'
                      alt='Dan Abramov'/>
              </Center>
            </AccordionPanel>
        </AccordionItem>

        <AccordionItem>
          <h2>
            <AccordionButton _expanded={{ bg: 'gray', color: 'white' }}>
                <Box as="span" flex='1' textAlign='center'>
                  <Heading>Contact Us</Heading>
                </Box>
                <AccordionIcon />
            </AccordionButton>
          </h2>
            <AccordionPanel pb={4}>
              <Text fontSize='2xl'color='azure'>You can reach me out at any moment on cocktaildb@cocktails.com</Text>
              </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </ChakraProvider>
</div>
  )
}

export default AboutUs