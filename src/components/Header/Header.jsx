import React from 'react'

const Header = () => {
  return (
    <div className = "header">
      <div><h2>My Cocktail DB</h2></div>
        <div><button className="btn btn-dark">Login</button></div>
    </div>
  )
}

export default Header