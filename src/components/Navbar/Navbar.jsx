import { Route, Routes, Switch, Link, BrowserRouter as Router } from "react-router-dom";

import React from 'react';
import './Navbar.css'



const Navbar = () => {

  return (

    <div className = "navegator">
      <Link to="/search-drink" className="button-zero"><button className="btn-width btn btn-dark">Search drink</button></Link>
      <Link to="/alcoholic" className="button-one"><button className='btn-width btn btn-dark'>Alcoholic</button></Link>
      <Link to="/non-alcoholic" className="button-two"><button className="btn-width btn btn-dark">Non Alcoholic</button></Link>
      <Link to="/champagne-glass" className="button-three"><button className="btn-width btn btn-dark">Champagne Glass</button></Link>
      <Link to="/cocktail-glass" className="button-four"><button className="btn-width btn btn-dark">Cocktail Glass</button></Link>  
      <Link to="/about-us" className="button-five"><button className="btn-width btn btn-secondary">About Us</button></Link>
      <Link to="/" className="button-six"><button className="btn-width btn btn-secondary">Register</button></Link> 
      <Link to="/" className="button-seven"><button className="btn-width btn btn-secondary">Home</button></Link>   
    </div>

  )
}

export default Navbar