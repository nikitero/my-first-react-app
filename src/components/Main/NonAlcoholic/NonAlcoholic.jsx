import React, { useState, useEffect } from "react";


const NonAlcoholic = () => {

  
  let [cocktails, setCocktail] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  
    useEffect(() => {
      setIsLoading(true);
      fetch('https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic')
        .then(response => response.json())
        .then(data => setCocktail(data.drinks))
        .catch(err=>console.log(err))
        .finally(() => setIsLoading(false));
    }, []); 


  const loading = (isLoading) ? 'Loading...' : null;
  const [detail, setDetail] = useState(null);
  

  const showDetails = (event) => {


        fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${event.target.id}`)
        .then(response => response.json())
        .then(data => setCocktail(data.drinks))
        .then(setDetail(true))
        .catch(err=>console.log(err))
  }

  const reloader = () => {
    window.location.reload(false)
  }

  return (
<div className="main">
<div className="alco_container drinks">
  {loading}
  {detail === null ? (
  cocktails.map((cocktail, key) => (
      <div className="cocktail-card"  key={ key }>
          <h5 onClick={showDetails} id={cocktail.idDrink}  className="drink-name" >{ cocktail.strDrink }</h5>
          <img src={cocktail.strDrinkThumb} alt="alcoCocktails" className = "image"/>      
      </div>))
      ) : (
        cocktails.map((cocktail, key) => (
      <div className="cocktail-card__detailed"  key={ key }>
          <h5  id={cocktail.idDrink} className="drink-name" >{ cocktail.strDrink }</h5>
          <img src={cocktail.strDrinkThumb} alt="alcoCocktails" className = "image"/>
          <h5 className="desc-title">Glass Type:<p className="desc-text">{cocktail.strGlass}</p></h5>      
          <h5 className="desc-title">First Ingredient:<p className="desc-text">{cocktail.strIngredient1}</p></h5>      
          <h5 className="desc-title">Second Ingredient: <p className="desc-text">{cocktail.strIngredient2}</p></h5>
          <h5 className="desc-title">Instructions: <p className="desc-text">{cocktail.strInstructions}</p></h5>
          <button onClick={reloader} className="btn btn-dark">Back</button>      
      </div>)) 

        
      )}

</div>
</div>
  )

}

export default NonAlcoholic