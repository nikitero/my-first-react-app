import React from 'react'
import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import './Main.css'
import Alcoholic from './Alcoholic/Alcoholic';
import NonAlcoholic from './NonAlcoholic/NonAlcoholic';
import ChampagneGlass from './ChampagneGlass/ChampagneGlass';
import CocktailGlass from './CocktailGlass/CocktailGlass';
import NoDrinks from './NoDrinks/NoDrinks';
import AboutUs from '../AboutUs/AboutUs';
import Search from './Search/Search';

const Main = () => {



  return (

  <div className="main">
  <Routes>
    <Route path="/search-drink" element= {<Search/>} />
    <Route path="/" element= {<NoDrinks/>} />
    <Route path="/alcoholic" element= {<Alcoholic/>} />
    <Route path="/non-alcoholic" element={<NonAlcoholic/>} />
    <Route path="/champagne-glass" element={<ChampagneGlass/>} />
    <Route path="/cocktail-glass" element={<CocktailGlass/>} />
    <Route path="/about-us" element={<AboutUs/>} />
 </Routes>
 </div>
  
  )
}


export default Main