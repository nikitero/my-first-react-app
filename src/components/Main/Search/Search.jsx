import React, { useState } from 'react';


const Search = () => {

let [cocktails, setCocktail] = useState([]);

const [formValue, setFormValue] = useState(null);

//Passing input value to formValue
const handleInput = (ev) => {
    setFormValue(ev.target.value); 
};
//Button onClick function receives event value in formValue and passes it to the API string
 const searcher = () => {
    console.log(formValue)

    fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${formValue}`)
    .then(response => response.json())
    .then((data) => {
        setCocktail(data.drinks)
        console.log(data.drinks) })   
    .catch(err=>console.log(err))
 }

//variable and function to trigger details when clicking on the cocktail title
const [detail, setDetail] = useState(null);

const showDetails = (event) => {


        fetch(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${event.target.id}`)
        .then(response => response.json())
        .then(data => setCocktail(data.drinks))
        .then(setDetail(true))
        .catch(err=>console.log(err))
  }

//Button back
  const reloader = () => {
    window.location.reload(false)
  }


  return (
<div className="search-bar">
    <div className="form">
            <input onChange={handleInput} type="text" className="form-control" placeholder="Find your drink" aria-label="Find your drink" aria-describedby="button-addon2"/>
            <button onClick={searcher} className="btn btn-outline-secondary" type="button" id="button-addon2">Button</button>
    </div>
        {detail === null ? (
        <div className="cocktail-list">
        {cocktails.map((cocktail, key) => (
            <div className="cocktail-card"  key={ key }>
                <h5 onClick={showDetails} id={cocktail.idDrink}  className="drink-name" >{ cocktail.strDrink }</h5>
                <img src={cocktail.strDrinkThumb} alt="alcoCocktails" className = "image"/>      
        </div>))}
        </div>
        ) : (
        cocktails.map((cocktail, key) => (
        <div className="cocktail-card__detailed"  key={ key }>
            <h5  id={cocktail.idDrink} className="drink-name" >{ cocktail.strDrink }</h5>
            <img src={cocktail.strDrinkThumb} alt="alcoCocktails" className = "image"/>
            <h5 className="desc-title">Glass Type:<p className="desc-text">{cocktail.strGlass}</p></h5>      
            <h5 className="desc-title">First Ingredient:<p className="desc-text">{cocktail.strIngredient1}</p></h5>      
            <h5 className="desc-title">Second Ingredient: <p className="desc-text">{cocktail.strIngredient2}</p></h5>
            <h5 className="desc-title">Instructions: <p className="desc-text">{cocktail.strInstructions}</p></h5>
            <button onClick={reloader} className="btn btn-dark">Back</button>      
        </div>)) 
        )}
    </div>

  )
}

export default Search